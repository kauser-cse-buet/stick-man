﻿using StickManApplication.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        IMovementController movementController;
        public Form1()
        {
            InitializeComponent();
            movementController = new MovementController(panel1, panelLabel1);
            this.KeyPress += new KeyPressEventHandler(movementController.MovementActionHandler);
            controlDirectionLabel.Text = "How to Move" + Environment.NewLine +
                "Move up: press w" + Environment.NewLine +
                "Move left: press a" + Environment.NewLine +
                "Move right: press d" + Environment.NewLine +
                "Move down: press s" + Environment.NewLine;
        }
    }
}
