﻿using System;
using System.Windows.Forms;

namespace StickManApplication.Controllers
{
    class MovementController : IMovementController
    {
        private Label labelBody;
        private Panel panelBody;
        private int stepSize;

        public MovementController(Panel panel, Label label)
        {
            labelBody = label;
            panelBody = panel;
            stepSize = 10;
        }
        public void MoveDown()
        {
            labelBody.Text = "Down";
            if(labelBody.Top <= panelBody.Top)
                labelBody.Top += stepSize;
        }

        public void MoveLeft()
        {
            labelBody.Text = "Left";
            if (labelBody.Left >= panelBody.Left)
                labelBody.Left -= stepSize;
        }

        public void MoveRight()
        {
            labelBody.Text = "Right";
            if ((labelBody.Left + labelBody.Width) < (panelBody.Left + panelBody.Width))
            {
                labelBody.Left += stepSize;
            }
        }

        public void MoveUp()
        {
            labelBody.Text = "Up";
            if ((labelBody.Top - labelBody.Height) > (panelBody.Top - panelBody.Height))
            {
                labelBody.Top -= stepSize;
            }
        }

        public void MovementActionHandler(object sender, KeyPressEventArgs e)
        {
            labelBody.Text = e.KeyChar.ToString();
            switch (e.KeyChar)
            {
                case 'w':
                    MoveUp();
                    break;
                case 'a':
                    MoveLeft();
                    break;
                case 'd':
                    MoveRight();
                    break;
                case 's':
                    MoveDown();
                    break;
            }
        }
    }

    interface IMovementController
    {
        void MoveUp();
        void MoveDown();
        void MoveLeft();
        void MoveRight();
        void MovementActionHandler(object sender, KeyPressEventArgs e);
    }
}
