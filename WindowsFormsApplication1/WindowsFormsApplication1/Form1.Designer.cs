﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.controlDirectionLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelLabel1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // controlDirectionLabel
            // 
            this.controlDirectionLabel.AutoSize = true;
            this.controlDirectionLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.controlDirectionLabel.Location = new System.Drawing.Point(24, 20);
            this.controlDirectionLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.controlDirectionLabel.Name = "controlDirectionLabel";
            this.controlDirectionLabel.Size = new System.Drawing.Size(37, 22);
            this.controlDirectionLabel.TabIndex = 1;
            this.controlDirectionLabel.Text = "text";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.panelLabel1);
            this.panel1.Location = new System.Drawing.Point(24, 146);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(379, 245);
            this.panel1.TabIndex = 2;
            // 
            // panelLabel1
            // 
            this.panelLabel1.AutoSize = true;
            this.panelLabel1.Location = new System.Drawing.Point(162, 92);
            this.panelLabel1.Name = "panelLabel1";
            this.panelLabel1.Size = new System.Drawing.Size(51, 20);
            this.panelLabel1.TabIndex = 0;
            this.panelLabel1.Text = "label3";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(426, 403);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.controlDirectionLabel);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label controlDirectionLabel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label panelLabel1;
    }
}

